<?php
require __DIR__ . '/includes/header.php';
require __DIR__ . '/includes/utils.php';
require __DIR__ . '/includes/costants.php';
if (isset($_SESSION['user'])) {
    header('Location: ' . BASE_URL . 'index.php');
}
?>

    <div class="mt-3"><h1>Accedi</h1></div>
    <?php
if (isset($_GET['stato'])) {
    \DataHandling\Utils\show_alert($_GET['stato'], $_GET['messages']);
}
?>
    <form method="POST" action="includes/login.php" class="container">
      <div class="col">
        <label for="username" class="form-label">Username</label>
        <input type="text" name="username" id="username" class="form-control" required>
      </div>
      <div class="col">
        <label for="password" class="form-label">Password</label>
        <input type="password" name="password" id="password" class="form-control" required>
      </div>
      <div class="col mt-3">
        <input type="submit" class="btn btn-outline-primary" value="Accedi">
      </div>
    </form>
  </main>
</body>
</html>
