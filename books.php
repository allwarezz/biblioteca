<?php
include_once 'includes/globals.php';
include_once 'includes/acl-admin.php';
?>
<?php
if (isset($_GET['stato'])) {
    \DataHandling\Utils\show_alert($_GET['stato'], $_GET['messages']);
}

if(isset($_GET['id'])){
  $book = \DataHandling\Books::selectData(null, $_GET['id']);
}
?>

<?php if (isset($_GET['id']) && isset($_GET['form']) && $_GET['form'] === 'modifica'): ?>
    <h2 class="mt-3">Aggiorna Libro</h2>
    <form class="row g-3 mt-3" method="POST" action="includes/books-router.php?action=update">
    <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>">
    <?php elseif (isset($_GET['id']) && isset($_GET['form']) && $_GET['form'] === 'visualizza'): ?>
      <h2 class="mt-3">Dettaglio Libro</h2>
      <form class="row g-3 mt-3">
    <?php elseif (!isset($_GET['id'])): ?>
        <h2 class="mt-3">Aggiungi Libro</h2>
    <form class="row g-3 mt-3" method="POST" action="includes/books-router.php?action=add">
    <?php endif;?>
  <div class="col-md-6">
    <label for="title" class="form-label">Titolo*</label>
    <input type="text" class="form-control" name="title" id="title" required autocomplete="off"
    <?php if (isset($_GET['id'])) : ?>
      value="<?php echo $book[0]['title'] ?>"
      <?php if (isset($_GET['form']) && $_GET['form'] === 'visualizza') : ?>
       disabled
      <?php endif; ?>
    <?php endif; ?>
    >
  </div>
  <div class="col-md-6">
    <label for="author" class="form-label">Autore*</label>
    <input type="text" class="form-control" name="author" id="author" required autocomplete="off"
    <?php if (isset($_GET['id'])) : ?>
      value="<?php echo $book[0]['author'] ?>"
      <?php if (isset($_GET['form']) && $_GET['form'] === 'visualizza') : ?>
       disabled
      <?php endif; ?>
    <?php endif; ?>
    >
  </div>
  <div class="col-6">
    <label for="isbn" class="form-label">ISBN*</label>
    <input type="text" class="form-control" name="isbn" id="isbn" maxlength="13" minlength="13" required autocomplete="off"
    <?php if (isset($_GET['id'])) : ?>
      value="<?php echo $book[0]['isbn'] ?>"
      <?php if (isset($_GET['form']) && $_GET['form'] === 'visualizza') : ?>
       disabled
      <?php endif; ?>
    <?php endif; ?>
    >
  </div>
  <div class="col-6">
    <label for="published" class="form-label">Anno Pubblicazione</label>
    <input type="number" class="form-control" name="published" id="published"
    <?php if (isset($_GET['id'])) : ?>
      value="<?php echo $book[0]['published'] ?>"
      <?php if (isset($_GET['form']) && $_GET['form'] === 'visualizza') : ?>
       disabled
      <?php endif; ?>
    <?php endif; ?>
    >
  </div>
  <div class="col-md-12">
    <label for="description" class="form-label">Descrizione</label>
    <textarea class="form-control" name="description"
    <?php if (isset($_GET['form']) && $_GET['form'] === 'visualizza') : ?>
       disabled
      <?php endif; ?>
    ><?php if (isset($_GET['id'])) : ?><?php echo $book[0]['description'] ?><?php endif; ?></textarea>
  </div>
  <small>Con l'asterisco (*) i campi obbligatori</small>
  <div class="col-md-2 offset-md-10">
  <?php if (isset($_GET['id']) && isset($_GET['form']) && $_GET['form'] === 'modifica'): ?>
    <button type="submit" class="btn btn-primary w-100">Modifica</button>
    <?php elseif (isset($_GET['id']) && isset($_GET['form']) && $_GET['form'] === 'visualizza'): ?>

    <?php elseif (!isset($_GET['id'])): ?>
        <button type="submit" class="btn btn-primary w-100">Salva</button>
    <?php endif;?>
  </div>
</form>







</main>
</body>
</html>
