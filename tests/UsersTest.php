<?php declare (strict_types = 1);

use \PHPUnit\Framework\TestCase;

final class UsersTest extends TestCase
{
    public function testLogin(): array
    {
        $result = \DataHandling\Users::loginUser(['username' => 'admin ', 'password' => 'admin']);
        $this->assertIsArray($result);

        return $result;
    }

}
