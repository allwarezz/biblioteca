<?php
include_once 'includes/globals.php';
include_once 'includes/acl-admin.php';
?>
<?php
$utenti = \DataHandling\Users::selectData();
$utenteScelto = [];
?>

<form class="mt-3">
    <div class="row">
        <div class="col-12">
            <label for="associato">Associati</label>
            <input class="form-control" list="associati" id="associato" name="associato" autocomplete="off" />
            <datalist id="associati">
                <?php
foreach ($utenti as $utente) {
    if (isset($_GET['associato']) && $utente['username'] === $_GET['associato']) {
        $utenteScelto = $utente;
    }
    $type = ($utente['is_admin']) ? 'Admin' : 'Associato';
    echo "<option value='$utente[username]'>$utente[lastname] $utente[firstname] ($type)</option>";
}
?>
            </datalist>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-2 offset-10">
            <input class="btn btn-primary w-100" type="submit" value="Seleziona">
        </div>
    </div>
</form>
<hr />
<?php
if (isset($_GET['stato'])) {
    \DataHandling\Utils\show_alert($_GET['stato'], $_GET['messages']);
}
?>
<br/>
<?php if ($utenteScelto): ?>
    <form class="mt-3" method="POST" action="./includes/users-router.php?action=update">
        <input type="hidden" name="id" value="<?php echo $utenteScelto['id'] ?>">
<?php else: ?>
    <form class="mt-3" method="POST" action="./includes/users-router.php?action=add">
<?php endif;?>
    <div class="row">
        <div class="col-3">
        <label for="username">Username *</label>
            <input type="text" class="form-control" name="username"
            <?php if ($utenteScelto): ?>
            readonly
            value="<?php echo $utenteScelto['username'] ?>"
            <?php else: ?>
                required
            <?php endif;?>
            autocomplete=off
            >
        </div>
        <div class="col-3">
        <label for="password">Password **</label>
            <input type="password" class="form-control" name="password"
            <?php if (!$utenteScelto): ?>
            required
            <?php endif;?>
            autocomplete=off
            >
        </div>
        <div class="col-3">
        <label for="repassword">Re-Password **</label>
            <input type="password" class="form-control" name="repassword"
            <?php if (!$utenteScelto): ?>
            required
            <?php endif;?>
            autocomplete=off
            >
        </div>
        <div class="col-3">
        <label for="is_admin">Tipo *</label>
            <select class="form-control" name="is_admin">
                <option value="0" <?php echo ($utenteScelto && $utenteScelto['is_admin']) ? '' : 'selected'; ?> >Associato</option>
                <option value="1" <?php echo ($utenteScelto && $utenteScelto['is_admin']) ? 'selected' : ''; ?> >Admin</option>
            </select>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-3">
        <label for="lastname">Cognome *</label>
            <input type="text" class="form-control" name="lastname"
            <?php if ($utenteScelto): ?>
            value="<?php echo $utenteScelto['lastname'] ?>"
            <?php endif;?>
            required
            autocomplete=off
            >
        </div>
        <div class="col-3">
        <label for="firstname">Nome *</label>
            <input type="text" class="form-control" name="firstname"
            <?php if ($utenteScelto): ?>
            value="<?php echo $utenteScelto['firstname'] ?>"
            <?php endif;?>
            required
            autocomplete=off
            >
        </div>
        <div class="col-3">
        <label for="email">Email</label>
            <input type="email" class="form-control" name="email"
            <?php if ($utenteScelto): ?>
            value="<?php echo $utenteScelto['email'] ?>"
            <?php endif;?>
            autocomplete=off
            >
        </div>
        <div class="col-3">
        <label for="phone">Telefono</label>
            <input type="text" class="form-control" name="phone"
            <?php if ($utenteScelto): ?>
            value="<?php echo $utenteScelto['phone'] ?>"
            <?php endif;?>
            >
        </div>
    </div>
    <div class="mt-3"><small>Con l'astisco (*) i campi obbligatori</small></div>
    <div><small>** obbligatori solo per nuovi associati</small></div>
    <div class="row mt-3">
        <?php if ($utenteScelto): ?>
            <div class="col-2 offset-6">
                <a href="./users.php" class="btn btn-outline-info w-100">Nuovo</a>
            </div>
            <div class="col-2">
                <a href="./includes/users-router.php?action=delete&id=<?php echo $utenteScelto['id'] ?>" class="btn btn-outline-danger w-100">Elimina</a>
            </div>
            <div class="col-2">
                <input class="btn btn-primary w-100" type="submit" value="Modifica">
            </div>
        <?php else: ?>
            <div class="col-2 offset-10">
                <input class="btn btn-primary w-100" type="submit" value="Salva">
            </div>
        <?php endif;?>
    </div>
</form>