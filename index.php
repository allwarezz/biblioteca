<?php
include_once 'includes/globals.php';
if (isset($_GET['stato'])) {
  \DataHandling\Utils\show_alert($_GET['stato'], $_GET['messages']);
}

$books = null;
if(isset($_GET['action']) && $_GET['action'] === 'doSearch'){
  $books = \DataHandling\Books::selectData($_POST);
}
?>

<div class="card mt-3">
  <div class="card-body">
      <h2>Ricerca</h2>
  <form class="row g-3" method="POST" action="./index.php?action=doSearch">
  <div class="col-md-6">
    <label for="title" class="form-label">Titolo</label>
    <input type="text" class="form-control" name="title" id="title" autocomplete="off">
  </div>
  <div class="col-6">
    <label for="author" class="form-label">Autore</label>
    <input type="text" class="form-control" name="author" id="author" autocomplete="off">
  </div>
  <div class="col-12">
    <div class="form-check">
      <input class="form-check-input" type="checkbox" name="available" id="available">
      <label class="form-check-label" for="gridCheck">
        Solo disponibili
      </label>
    </div>
  </div>
  <div class="col-2 offset-10">
    <button type="submit" class="btn btn-primary w-100"><i class="bi bi-search"></i> Cerca</button>
  </div>
</form>
  </div>
</div>
<br/>
<?php if(is_null($books)): ?>
  <div class="alert alert-info text-center" role="alert">Cerca un Libro!!</div>
<?php elseif(count($books) > 0): ?>
  <table class="table">
    <thead>
    <?php echo \DataHandling\Utils\get_table_head_books($books[0]); ?>
    </thead>
    <tbody>
    <?php echo \DataHandling\Utils\get_table_body_books($books); ?>
    </tbody>
</table>
<?php else: ?>
  <div class="alert alert-info text-center" role="alert">Nessun Libro Trovato!</div>
<?php endif; ?>
</main>
</body>
</html>
