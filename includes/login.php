<?php
session_start();
include_once __DIR__ . '/utils.php';
include_once __DIR__ . '/DBConnection.php';
include_once __DIR__ . '/FormHandle.php';
include_once __DIR__ . '/Users.php';
include_once __DIR__ . '/costants.php';

if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['user']);
    header('Location: ' . BASE_URL . 'login.php');
    exit;
}

$loggedInUser = \DataHandling\Users::loginUser($_POST);
$_SESSION['user'] = $loggedInUser;
header('Location: ' . BASE_URL);
exit;
