<?php
namespace DataHandling;

use \DataHandling\Utils\InputSanitize;

class Books extends FormHandle
{
    use \DataHandling\Utils\InputSanitize;

    public static function insertData($form_data, $id = null)
    {

        $fields = self::sanitize($form_data);
        if($fields['isbn'] == '' || $fields['title'] == '' || $fields['author'] == '' ){
            header('Location: ' . BASE_URL . 'books.php?stato=errore&messages=I campi  con l\'asterisco (*) sono obbligatori');
            exit;
        }
        $mysqli = \DBHandle\getConnection();

        $query = $mysqli->prepare('INSERT INTO books(isbn, title, author, description, published, is_deleted) VALUES (?, ?, ?, ?, ?, ?)');
        $desc = ($fields['description'] !== '') ? $fields['description'] : null;
        $published = ($fields['published'] !== '') ? intval($fields['published']) : null;
        $isDel = 0;
        $query->bind_param('ssssii', $fields['isbn'], $fields['title'], $fields['author'], $desc, $published, $isDel);
        $query->execute();
        if ($query->affected_rows === 0) {
            error_log('Errore MySQL: ' . $query->error_list[0]['error']);
            header('Location: ' . BASE_URL . 'books.php?stato=errore&messages=Errore nell\'inserimento del libro');
            exit;
        }

        if ($query->affected_rows < 0) {

            if ($query->errno === 1062 && $query->sqlstate == '23000') {
                header('Location: ' . BASE_URL . 'books.php?stato=errore&messages=Libro già presente');
                exit;
            }
            error_log('Errore MySQL: ' . $query->error_list[0]['error']);
            header('Location: ' . BASE_URL . 'books.php?stato=errore&messages=Errore nell\'inserimento del libro');
            exit;
        }

        header('Location: ' . BASE_URL . 'books.php?stato=success&messages=Libro Inserito Correttamente');
        exit;
    }

    public static function selectData($args = null, $id = null)
    {
        $mysqli = \DBHandle\getConnection();

        if($id) {
            $id = intval($id);
            // il PREPARE non funzionava
            $res = $mysqli->query("SELECT * FROM books WHERE id = " . $id);
        }else{
            $fields = self::sanitize($args);
            $where = [];
            foreach($fields as $k => $v) {
                if($k !== 'available') {
                    $where[] = $k . " LIKE '%" . $v . "%'";
                }elseif($v === 'on'){
                    $where[] = 'is_rented = 0';
                    $where[] = 'is_deleted = 0';
                }
            }
            // il PREPARE non funzionava
            $res = $mysqli->query("SELECT books.`*`, users.username FROM books LEFT JOIN (SELECT user_id, books_id FROM book_rentals WHERE book_rentals.return_date IS NULL) br
                                    ON books.id = br.books_id LEFT JOIN users ON users.id = br.user_id WHERE " . implode(' AND ', $where));
        }
        $results = [];
        while ($row = $res->fetch_assoc()) {
            $row['title'] = stripslashes($row['title']);
            $row['author'] = stripslashes($row['author']);
            if (isset($row['description'])) {
                $row['description'] = stripslashes($row['description']);
            }
            $results[] = $row;
        }
        return $results;
    }

    public static function updateData($form_data, $id){
        $fields = self::sanitize($form_data);
        $id = intval($id);
        $mysqli = \DBHandle\getConnection();
        if(!isset($fields['is_deleted']) || !isset($fields['is_rented'])){
            $query = $mysqli->prepare('UPDATE books SET isbn = ?, title = ?, author = ?, description = ?, published = ? WHERE id = ?');
            $desc = ($fields['description'] !== '') ? $fields['description'] : null;
            $published = ($fields['published'] !== '') ? intval($fields['published']) : null;
            $query->bind_param('ssssii', $fields['isbn'], $fields['title'], $fields['author'], $desc, $published, $id);
            $query->execute();
        }

        if ($query->affected_rows === 0) {
            error_log('Errore MySQL: ' . $query->error_list[0]['error']);
            header('Location: ' . BASE_URL . 'books.php?stato=errore&messages=Errore nell\'aggiornamento del libro');
            exit;
        }

        if ($query->affected_rows < 0) {

            if ($query->errno === 1062 && $query->sqlstate == '23000') {
                header('Location: ' . BASE_URL . 'books.php?stato=errore&messages=Isbn già presente ');
                exit;
            }
            error_log('Errore MySQL: ' . $query->error_list[0]['error']);
            header('Location: ' . BASE_URL . 'books.php?stato=errore&messages=Errore nell\'aggiornamento del libro');
            exit;
        }

        header('Location: ' . BASE_URL . 'books.php?stato=success&messages=Libro Aggiornato Correttamente');
        exit;
    }

    public static function deleteData($id){
        $id = intval($id);
        $mysqli = \DBHandle\getConnection();

        $query = $mysqli->prepare('UPDATE books SET is_deleted = 1 WHERE id = ?');
        $query->bind_param('i', $id);
        $query->execute();

        if ($query->affected_rows === 0) {
            error_log('Errore MySQL: ' . $query->error_list[0]['error']);
            header('Location: ' . BASE_URL . 'index.php?stato=errore&messages=Errore nell\'eliminazione del libro');
        }

        header('Location: ' . BASE_URL . 'index.php?stato=success&messages=Libro Eliminato Correttamente');
        exit;
    }

    public static function undoDelete($id){
        $id = intval($id);
        $mysqli = \DBHandle\getConnection();

        $query = $mysqli->prepare('UPDATE books SET is_deleted = 0 WHERE id = ?');
        $query->bind_param('i', $id);
        $query->execute();

        if ($query->affected_rows === 0) {
            error_log('Errore MySQL: ' . $query->error_list[0]['error']);
            header('Location: ' . BASE_URL . 'index.php?stato=errore&messages=Errore nel reinserimento del libro');
        }

        header('Location: ' . BASE_URL . 'index.php?stato=success&messages=Libro Reinserito Correttamente');
        exit;
    }

    protected static function sanitize($fields)
    {
        foreach ($fields as $k => $v) {
            $fields[$k] = self::cleanInput($v);
        }

        return $fields;
    }
}
