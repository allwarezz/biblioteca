<?php
namespace DataHandling;

use \DataHandling\Utils\InputSanitize;

class Rents extends FormHandle
{
    use \DataHandling\Utils\InputSanitize;

    public static function insertData($form_data, $id = null)
    {
        $fields = self::sanitize($form_data);
        if($fields['associato'] == ''){
            header('Location: ' . BASE_URL . 'rents.php?idBook='.$fields['idBook'].'&stato=errore&messages=Devi selezionare un associato');
            exit;
        }

        $mysqli = \DBHandle\getConnection();

        $user = \DataHandling\Users::selectData(['username' => $fields['associato']]);
        /* Inizia la transazione */
        $mysqli->begin_transaction();
        try{
            $idBook = intval($fields['idBook']);
            $mysqli->query("UPDATE books SET is_rented = 1 WHERE id = " . $idBook);

            $query = $mysqli->prepare('INSERT INTO book_rentals(books_id, user_id) VALUES (?, ?)');
            $idUser = intval($user[0]['id']);
            $query->bind_param('ii', $idBook, $idUser);
            $query->execute();
            if ($query->affected_rows === 0) {
                error_log('Errore MySQL: ' . $query->error_list[0]['error']);
                header('Location: ' . BASE_URL . 'rents.php?idBook='.$fields['idBook'].'&stato=errore&messages=Errore non è possibile noleggiare il libro');
                exit;
            }

            $mysqli->commit();
        }catch (mysqli_sql_exception $exception) {
            $mysqli->rollback();
            error_log('Errore MySQL: ' . $exception->message);
            header('Location: ' . BASE_URL . 'rents.php?idBook='.$fields['idBook'].'&stato=errore&messages=Errore non è possibile noleggiare il libro');
            exit;
        }

        header('Location: ' . BASE_URL . 'index.php?stato=success&messages=Libro Noleggiato Correttamente');
        exit;
    }

    public static function selectData($args = null, $id = null)
    {
        $mysqli = \DBHandle\getConnection();
        if(is_null($args) && is_null($id)){
            $res = $mysqli->query("SELECT * FROM book_rentals JOIN books ON books.id = book_rentals.books_id WHERE user_id = ".$_SESSION['user']['id']." ORDER BY book_rentals.withdrawal_date DESC");
        }elseif(is_null($args) && !is_null($id)){
            $id = intval($id);
            $res = $mysqli->query("SELECT * FROM book_rentals JOIN books ON books.id = book_rentals.books_id JOIN users ON users.id = book_rentals.user_id WHERE books_id = ".$id." ORDER BY book_rentals.withdrawal_date DESC");
        }

        $results = [];
        while ($row = $res->fetch_assoc()) {
            $results[] = $row;
        }

        return $results;
    }

    public static function updateData($data, $id){

        $idBook = intval($id);

        $mysqli = \DBHandle\getConnection();
        
        $mysqli->begin_transaction();
        try{

            $mysqli->query("UPDATE books SET is_rented = 0 WHERE id = " . $idBook);
            $now = date('Y-m-d H:i:s');
            $query = $mysqli->prepare('UPDATE book_rentals SET return_date = \'' . $now . '\' WHERE return_date IS NULL AND books_id = ?');
            $query->bind_param('i', $idBook);
            $query->execute();
            if ($query->affected_rows === 0) {
                error_log('Errore MySQL: ' . $query->error_list[0]['error']);
                header('Location: ' . BASE_URL . 'index.php?&stato=errore&messages='
                .'Errore non è possibile segnalare la restituzione del libro');
                exit;
            }

            $mysqli->commit();
        }catch (mysqli_sql_exception $exception) {
            $mysqli->rollback();
            error_log('Errore MySQL: ' . $exception->message);
            header('Location: ' . BASE_URL . 'index.php?stato=errore&messages='
            .'Errore non è possibile segnalare la restituzione del libro');
            exit;
        }

        header('Location: ' . BASE_URL . 'index.php?stato=success&messages=Libro Segnalato come Restituito!');
        exit;
    }

    public static function deleteData($id){
        throw new Exception('Metodo Non Ancora Implementato');
    }

    protected static function sanitize($fields)
    {
        foreach ($fields as $k => $v) {
            $fields[$k] = self::cleanInput($v);
        }

        return $fields;
    }
}
