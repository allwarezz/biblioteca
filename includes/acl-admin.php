<?php

$uri_fragments = explode("/", $_SERVER['SCRIPT_FILENAME']);
if ((!isset($_SESSION['user']) || !$_SESSION['user']['is_admin']) ) {
    header('Location: ' . BASE_URL . 'index.php?stato=errore&messages=Non Autorizzato');
}
