<?php
include_once __DIR__ . './globals.php';
include_once 'acl-admin.php';

if (!isset($_GET['action'])) {
    header('Location: ' . BASE_URL . 'index.php');
}

switch ($_GET['action']) {
    case 'add':
        \DataHandling\Books::insertData($_POST);
        break;
    case 'update':
        \DataHandling\Books::updateData($_POST, $_POST['id']);
        break;
    case 'delete':
        \DataHandling\Books::deleteData($_GET['id']);
        break;
    case 'undo-delete':
        \DataHandling\Books::undoDelete($_GET['id']);
        break;
    default:
        header('Location: ' . BASE_URL . 'index.php');
}
