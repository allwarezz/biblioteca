<?php
namespace DataHandling\Utils;


function get_table_head_books($assoc_array)
{
    $keys = array_keys($assoc_array);
    $html = '';

    foreach ($keys as $key) {
        switch($key){
            case 'title':
                $html .= '<th scope="col">Titolo</th>';
                break;
            case 'author':
                $html .= '<th scope="col">Autore</th>';
                break;
            case 'published':
                $html .= '<th scope="col">Anno</th>';
                break;
        }
    }

    $html .= '<th>Disponibilità</th>';
    if((isset($_SESSION['user']) && $_SESSION['user']['is_admin'])){
        $html .= '<th>Username</th><th></th>';
    }
    return $html;
}

function get_table_body_books($items)
{
    $html = '';
    $i = 0;
    $not_visible = ['isbn','description','username'];
    foreach ($items as $row) {
        if(!$row['is_deleted'] || (isset($_SESSION['user']) && $_SESSION['user']['is_admin'])){
            $html .= '<tr>';
            $id = 0;
            $available = 'available';
            $username = $row['username'];
            foreach ($row as $key => $value) {
                if(in_array($key, $not_visible)){
                    continue;
                }
                if($key === 'id'){
                    $id = $value;
                    continue;
                }
                if($key === 'is_rented'){
                    if($value){
                        $available = 'rented';
                    }
                    continue;
                }

                if($key === 'is_deleted'){
                    if($value){
                        $available = 'deleted';
                    }
                    continue;
                }
                if($key === 'published' && is_null($value)){
                    $html .= '<td><i>n.d.</i></td>';
                    continue;
                }
                $html .= '<td>'.$value.'</td>';

            }
            if($available === 'available'){
                $html .= '<td>' . '<span class="dot-available"></span>';
            }elseif($available === 'rented'){
                $html .= '<td>' . '<span class="dot-not-available"></span>';
            }elseif($available === 'deleted'){
                $html .= '<td>' . '<span class="dot-not-available"></span>';
            }
            if((isset($_SESSION['user']) && $_SESSION['user']['is_admin'])){
                if($available === 'available'){
                    $html .= '</td><td> - </td>';
                    $html .= '<td><center>' . '<a href="./books.php?id='.$id.'&form=visualizza"><i class="bi bi-info-circle" style="color: grey;" title="visualizza"></i></a>&nbsp;'
                                    .'<a href="./books.php?id='.$id.'&form=modifica"><i class="bi bi-pencil" style="color: gold;" title="modifica"></i></a>&nbsp;'
                                    .'<a href="./rents.php?idBook='.$id.'"><i class="bi bi-bookmark-plus" style="color: green;" title="dai in prestito"></i></a>&nbsp;'
                                    .'<a href="./book-rents.php?id='.$id.'"><i class="bi bi-clock-history" style="color: blue;" title="cronologia noleggi"></i></a>&nbsp;'
                                    .'<a href="./includes/books-router.php?action=delete&id='.$id.'"><i class="bi bi-trash" style="color: red;" title="elimina"></i></a>' . '</center></td>';
                }elseif($available === 'rented'){
                    $html .= '</td><td>'.$username.'</td>';
                    $html .= '<td><center>' . '<a href="./books.php?id='.$id.'&form=visualizza"><i class="bi bi-info-circle" style="color: grey;" title="visualizza"></i></a>&nbsp;'
                                    .'<a href="./books.php?id='.$id.'&form=modifica"><i class="bi bi-pencil" style="color: gold;" title="modifica"></i></a>&nbsp;'
                                    .'<a href="./includes/rents-router.php?action=update&id='.$id.'""><i class="bi bi-bookmark-check" style="color: green;" title="restituisci"></i></a>&nbsp;'
                                    .'<a href="./book-rents.php?id='.$id.'"><i class="bi bi-clock-history" style="color: blue;" title="cronologia noleggi"></i></a>&nbsp;'
                                    .'<a href="./includes/books-router.php?action=delete&id='.$id.'"><i class="bi bi-trash" style="color: red;" title="elimina"></i></a>' . '</center></td>';
                }elseif($available === 'deleted'){
                    $html .= '</td><td> - </td>';
                    $html .= '<td><center>' .
                                '<a href="./includes/books-router.php?action=undo-delete&id='.$id.'"><i class="bi bi-plus-square" style="color: blue;" title="annulla cancellazione"></i></a>&nbsp;'
                            . '</center></td>';
                }
            }
            $html .= '</tr>';
        }
    }

return $html;
}

function get_table_head_history_rents($assoc_array)
{
    $keys = array_keys($assoc_array);
    $html = '';
    $cols = [];
    foreach ($keys as $key) {
        switch($key){
            case 'title':
                $cols[] = '<th scope="col">Titolo</th>';
                break;
            case 'author':
                $cols[] = '<th scope="col">Autore</th>';
                break;
            case 'withdrawal_date':
                $cols[] = '<th scope="col">Data Noleggio</th>';
                break;
            case 'username':
                $cols[] = '<td scope="col">Username</td>';
                break;
            case 'return_date':
                $cols[] = '<th scope="col">Data Restituzione</th>';
                break;
        }
    }
    $html .= $cols[2].$cols[3].$cols[0].$cols[1];
    if(isset($cols[4])){
        $html .= $cols[4];
    }
    return $html;
}

function get_table_body_history_rents($items)
{

    $html = '';
    foreach ($items as $row) {
        $html .= '<tr>';
        $cols = [];
        foreach($row as $k => $v){
            switch($k){
                case 'title':
                    $cols[] = '<td scope="col">'.$v.'</td>';
                    break;
                case 'author':
                    $cols[] = '<td scope="col">'.$v.'</td>';
                    break;
                case 'withdrawal_date':
                    $date_time = explode(' ', $v);
                    $date = implode('/',array_reverse(explode('-',$date_time[0])));
                    $cols[] = '<td scope="col">'.$date.'</td>';
                    break;
                case 'username':
                    $cols[] = '<td scope="col">'.$v.'</td>';
                    break;
                case 'return_date':
                    if(is_null($v)){
                        $v = 'Non Restituito';
                    }else{
                        $date_time = explode(' ', $v);
                        $v = implode('/',array_reverse(explode('-',$date_time[0])));
                    }
                    $cols[] = '<td scope="col">'.$v.'</td>';
                    break;
            }
        }
        $html .= $cols[2].$cols[3].$cols[0].$cols[1];
        if(isset($cols[4])){
            $html .= $cols[4];
        }
        $html .= '</tr>';
    }
    return $html;
}

function show_alert($state, $messages)
{

    if ($state === "errore") {
        echo '<div class="alert alert-danger mt-3" role="alert">' . $messages . '</div>';
    } elseif ($state === "success") {
        echo '<div class="alert alert-success mt-3" role="alert">' . $messages . '</div>';
    }
}

trait InputSanitize
{
    public static function cleanInput($data)
    {
        $data = trim($data);
        $data = filter_var($data, FILTER_SANITIZE_ADD_SLASHES);
        $data = filter_var($data, FILTER_SANITIZE_SPECIAL_CHARS);
        return $data;
    }
}
