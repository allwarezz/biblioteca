<?php
namespace DataHandling;

use \DataHandling\Utils\InputSanitize;

class Users extends FormHandle
{
    use \DataHandling\Utils\InputSanitize;

    public static function loginUser($form_data)
    {

        $fields = array(
            'username' => $form_data['username'],
            'password' => $form_data['password'],
        );

        $fields = self::sanitize($fields);

        $mysqli = \DBHandle\getConnection();

        $query = $mysqli->query("SELECT * FROM users WHERE username = '" . $fields['username'] . "'");

        if ($query->num_rows === 0) {
            header('Location:' . BASE_URL . 'login.php?stato=errore&messages=Utente non presente');
            exit;
        }

        $user = $query->fetch_assoc();

        if (!password_verify($fields['password'], $user['password'])) {
            header('Location: ' . BASE_URL . 'login.php?stato=errore&messages=Password errata');
            exit;
        }

        return array(
            'id' => $user['id'],
            'username' => $user['username'],
            'is_admin' => $user['is_admin'],
        );
    }

    public static function selectData($args = null, $id = null)
    {
        $mysqli = \DBHandle\getConnection();

        if (is_null($args) && is_null($id)) {
            $res = $mysqli->query("SELECT * FROM users");
            if ($res->num_rows === 0) {
                header('Location:' . BASE_URL . 'users.php?stato=errore&messages=Errore nelle ricerca degli utenti');
                exit;
            }
        } elseif (!is_null($args) && isset($args['username']) && is_null($id)) {
            // la PREPARE non funzionava
            $res = $mysqli->query("SELECT * FROM users WHERE username = '" . $args['username'] . "'");
            if ($res->num_rows === 0) {
                header('Location:' . BASE_URL . 'users.php?stato=errore&messages=Errore nelle ricerca degli utenti');
                exit;
            }
        }

        $results = [];
        while ($row = $res->fetch_assoc()) {
            $row['username'] = stripslashes($row['username']);
            $row['firstname'] = stripslashes($row['firstname']);
            $row['lastname'] = stripslashes($row['lastname']);
            $results[] = $row;
        }

        return $results;

    }

    public static function insertData($form_data, $id = null)
    {
        $fields = self::sanitize($form_data);
        if (!$fields || $fields['username'] === '' || $fields['firstname'] === '' || $fields['lastname'] === ''
            || $fields['password'] === '' || $fields['repassword'] === '') {
            header('Location: ' . BASE_URL . 'users.php?stato=errore&messages=Valorizzazione dei Campi NON Corretta');
            exit;
        }

        if ($fields['password'] !== $fields['repassword']) {
            header('Location: ' . BASE_URL . 'users.php?stato=errore&messages=Password e Re-Password devono essere uguali!');
            exit;
        }

        $mysqli = \DBHandle\getConnection();

        $query = $mysqli->prepare('INSERT INTO users(username, password, firstname, lastname, phone, email, is_admin) VALUES (?, ?, ?, ?, ?, ?, ?)');
        $phone = ($fields['phone'] !== '') ? $fields['phone'] : null;
        $email = ($fields['email'] !== '') ? $fields['email'] : null;
        $pwd = password_hash($fields['password'], PASSWORD_DEFAULT);
        $isAdmin = intval($fields['is_admin']);
        $query->bind_param('ssssssi', $fields['username'], $pwd, $fields['firstname'], $fields['lastname'], $phone, $email, $isAdmin);
        $query->execute();
        if ($query->affected_rows === 0) {
            error_log('Errore MySQL: ' . $query->error_list[0]['error']);
            header('Location: ' . BASE_URL . 'users.php?stato=errore&messages=Errore nell\'inserimento dell\'Associato');
            exit;
        }

        if ($query->affected_rows < 0) {

            if ($query->errno === 1062 && $query->sqlstate == '23000') {
                header('Location: ' . BASE_URL . 'users.php?stato=errore&messages=Username già utilizzato');
                exit;
            }
            error_log('Errore MySQL: ' . $query->error_list[0]['error']);
            header('Location: ' . BASE_URL . 'users.php?stato=errore&messages=Errore nell\'inserimento dell\'Associato');
            exit;
        }

        header('Location: ' . BASE_URL . 'users.php?stato=success&messages=Associato Inserito Correttamente');
        exit;
    }

    public static function updateData($form_data, $id)
    {
        $fields = self::sanitize($form_data);
        if (!$fields || $fields['firstname'] === '' || $fields['lastname'] === '') {
            header('Location: ' . BASE_URL . 'users.php?stato=errore&messages=Valorizzazione dei Campi NON Corretta');
            exit;
        }

        if (($fields['password'] !== '' || $fields['repassword'] !== '') && $fields['password'] !== $fields['repassword']) {
            header('Location: ' . BASE_URL . 'users.php?stato=errore&messages=Password e Re-Password devono essere uguali!');
            exit;
        }

        $mysqli = \DBHandle\getConnection();
        $phone = ($fields['phone'] !== '') ? $fields['phone'] : null;
        $email = ($fields['email'] !== '') ? $fields['email'] : null;
        $isAdmin = intval($fields['is_admin']);
        $id = intval($_POST['id']);
        $sql_query = 'UPDATE users SET password = ?, firstname = ?, lastname = ?, phone = ?, email = ?, is_admin = ? WHERE id = ?';
        if ($fields['password'] === '') {
            $sql_query = str_replace('password = ?,', '', $sql_query);
            $query = $mysqli->prepare($sql_query);
            $query->bind_param('ssssii', $fields['firstname'], $fields['lastname'], $phone, $email, $isAdmin, $id);
        } else {
            $query = $mysqli->prepare($sql_query);
            $pwd = password_hash($fields['password'], PASSWORD_DEFAULT);
            $query->bind_param('sssssii', $pwd, $fields['firstname'], $fields['lastname'], $phone, $email, $isAdmin, $id);
        }
        $query->execute();
        if ($query->affected_rows === 0) {
            error_log('Errore MySQL: ' . $query->error_list[0]['error']);
            header('Location: ' . BASE_URL . 'users.php?stato=errore&messages=Errore nell\'aggiornamento dell\'Associato');
            exit;
        }

        header('Location: ' . BASE_URL . 'users.php?stato=success&messages=Associato Aggiornato Correttamente');
        exit;
    }

    public static function deleteData($id)
    {
        $mysqli = \DBHandle\getConnection();
        $idUser = intval($id);
        $res = $mysqli->query("SELECT * FROM book_rentals WHERE return_date IS NULL AND user_id = " . $idUser);
        if ($res->num_rows > 0) {
            header('Location: ' . BASE_URL . 'users.php?stato=errore&messages=L\'associato non può essere eliminato perchè ha in noleggio ' . $res->num_rows . ' libro/i');
            exit;
        }
        $mysqli->close();
        $mysqli = \DBHandle\getConnection();
        $query = $mysqli->prepare('DELETE FROM users WHERE id = ?');
        $query->bind_param('i', $idUser);
        $query->execute();

        if ($query->affected_rows === 0) {
            error_log('Error MySQL: ' . $query->error_list[0]['error']);
            header('Location: ' . BASE_URL . 'users.php?stato=errore&messages=Errore nell\'eliminazione dell\'Associato');
            exit;
        }

        header('Location: ' . BASE_URL . 'users.php?stato=success&messages=Associato Eliminato Correttamente');
        exit;
    }

    protected static function sanitize($fields)
    {
        foreach ($fields as $k => $v) {
            if ($k === 'email' && $v !== '') {
                if ($fields[$k] = parent::isEmailAddressValid($v)) {
                    continue;
                } else {
                    return false;
                }
            }

            if ($k === 'phone' && $v !== '') {
                if (parent::isPhoneNumberValid($v)) {
                    continue;
                } else {
                    return false;
                }
            }
            $fields[$k] = self::cleanInput($v);
        }
        return $fields;
    }
}
