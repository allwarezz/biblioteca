<?php
include_once __DIR__ . './globals.php';
include_once 'acl-admin.php';

if (!isset($_GET['action'])) {
    header('Location: ' . BASE_URL . 'index.php');
}

switch ($_GET['action']) {
    case 'add':
        \DataHandling\Rents::insertData($_POST);
        break;
    case 'update':
        \DataHandling\Rents::updateData(null,$_GET['id']);
        break;
    default:
        header('Location: ' . BASE_URL . 'index.php');
}
