<?php
include_once 'includes/globals.php';
include_once 'includes/acl-admin.php';
?>
<?php
$rents = [];
if(isset($_GET['id'])){
    $rents = \DataHandling\Rents::selectData(null,$_GET['id']);
}

if($rents):
?>
<table class="table mt-3">
    <thead>
    <?php echo \DataHandling\Utils\get_table_head_history_rents($rents[0]); ?>
    </thead>
    <tbody>
    <?php echo \DataHandling\Utils\get_table_body_history_rents($rents); ?>
    </tbody>
</table>
<?php else: ?>
<div class="alert alert-info mt-3" role="alert">Questo libro non è mai stato noleggiato</div>
<?php endif; ?>