<?php
include_once 'includes/globals.php';
?>
<?php

$rents = \DataHandling\Rents::selectData();


if($rents):
?>
<table class="table mt-3">
    <thead>
    <?php echo \DataHandling\Utils\get_table_head_history_rents($rents[0]); ?>
    </thead>
    <tbody>
    <?php echo \DataHandling\Utils\get_table_body_history_rents($rents); ?>
    </tbody>
</table>
<?php else: ?>
<div class="alert alert-info mt-3" role="alert">Non hai ancora noleggiato libri</div>
<?php endif; ?>