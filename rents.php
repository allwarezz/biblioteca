<?php
include_once 'includes/globals.php';
include_once 'includes/acl-admin.php';
?>
<?php
$utenti = \DataHandling\Users::selectData();
$book = null;
if (isset($_GET['idBook'])) {
    $book = \DataHandling\Books::selectData(null, $_GET['idBook']);
    if (!$book || $book[0]['is_rented'] || $book[0]['is_deleted']) {
        header('Location: ' . BASE_URL . 'index.php?stato=errore&messages=Non è possibile accedere a questo libro');
        exit;
    }
} else {
    header('Location: ' . BASE_URL . 'index.php?stato=errore&messages=Deve essere selezionato un libro');
    exit;
}
$utenteScelto = [];
if (isset($_GET['stato'])) {
    \DataHandling\Utils\show_alert($_GET['stato'], $_GET['messages']);
}
?>
<h2 class="mt-3">Gestione Prestiti</h2>
<fieldset class="p-2" style="border: 1px solid grey">
  <legend>Libro</legend>
  <div class="row">
    <div class="col">
        <label>Titolo</label>
        <input class="form-control" type="text" value="<?php echo $book[0]['title'] ?>" disabled>
    </div>
    <div class="col">
        <label>Autore</label>
        <input class="form-control" type="text" value="<?php echo $book[0]['author'] ?>" disabled>
    </div>
    <div class="col">
        <label>Isbn</label>
        <input class="form-control" type="text" value="<?php echo $book[0]['isbn'] ?>" disabled>
    </div>
</fieldset>
<form class="mt-3" method="POST" action="./includes/rents-router.php?action=add">
    <div class="row">
        <input type="hidden" name="idBook" value="<?php echo $book[0]['id'] ?>">
        <div class="col-12">
            <label for="associato">Associati</label>
            <input class="form-control" list="associati" id="associato" name="associato" autocomplete="off" />
            <datalist id="associati">
                <?php
foreach ($utenti as $utente) {
    if (isset($_GET['associato']) && $utente['username'] === $_GET['associato']) {
        $utenteScelto = $utente;
    }
    $type = ($utente['is_admin']) ? 'Admin' : 'Associato';
    echo "<option value='$utente[username]'>$utente[lastname] $utente[firstname] ($type)</option>";
}
?>
            </datalist>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-2 offset-10">
            <input class="btn btn-primary w-100" type="submit" value="Presta">
        </div>
    </div>
</form>
<hr />
<br/>